import { Component } from 'react';
import './App.css';
import MiddleDiv from './pages/Middle.jsx';
import Home from './pages/Home.jsx';
import Profile from './pages/Profile.jsx';
import Goote from './pages/Goote.jsx';
import Notice from './pages/Notice.jsx';
import Footer from './components/Footer.jsx';
import Navigation from './components/Navigation.jsx';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

class App extends Component {
  render(){
    return(
      <div className="App">
        <BrowserRouter>
          <Navigation></Navigation>
          <MiddleDiv></MiddleDiv>
          {/* <Switch>
            <Route exact path="/"><Home></Home></Route>
            <Route path="/Goote"><Goote></Goote></Route>
            <Route path="/Profile"><Profile></Profile></Route>
            <Route path="/Notice"><Notice></Notice></Route>
          </Switch> */}
          <Footer></Footer>
        </BrowserRouter>
      </div>
    );
  }

}

export default App;
