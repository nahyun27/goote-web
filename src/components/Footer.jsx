import { Component } from 'react';
import '../styles/css/Footer.css';
import '../styles/css/common.css';

class Footer extends Component {
  render(){
    return(
      <div className="Footer">
        <div className="contents">
          <p className="terms">
            <a href="https://www.notion.so/2021-10-01-58255c27c221470787c85da7cc54afd5">이용약관 </a>
            |
            <a href="https://www.notion.so/2021-10-01-0830ee8d5ae54d85ad85dea4524c876c"> 개인정보 처리방침</a>
          </p>
          <p>주식회사 마이크로마우스 | 대표: 김병준</p>
          <p>서울특별시 서초구 강남대로 305, B117-49호 (서초동, 현대렉시온) | 사업자번호: 447-81-023-89</p>
          <p>대표 전화: 070-8776-7771 | 이메일: support@micromouse.co.kr</p>
          <p>© 2021 micromouse. All rights reserved.</p>
        </div>
      </div>
    );
  }
}


export default Footer;