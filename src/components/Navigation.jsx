import ball from '../assets/ball.svg';
import goote from '../assets/goote.svg';
import '../styles/css/Navigation.css';
import '../styles/css/common.css';
import { BrowserRouter, NavLink } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import { Link } from "react-scroll"

function Navigation() {

  const [scrollPosition, setScrollPosition] = useState(0);
  const updateScroll = () => {
      setScrollPosition(window.scrollY || document.documentElement.scrollTop);
  }
  useEffect(()=>{
      window.addEventListener('scroll', updateScroll);
  });
    return(
      <div className="Navigation">
        <nav className={scrollPosition < 100 ? "navbar" : "change-navbar"}>
          <BrowserRouter>
          <Link to="/" spy={true} smooth={true}>
          <div className="navbar__logo">
            <img src={ball} className="App-logo" alt="logo" />
            <img src={goote} className="App-logo-writing" alt="logo-writing" />
          </div>
          </Link>
            <ul className="navbar__menu">
              <li><Link to="/" spy={true} smooth={true}>홈</Link></li>
              <li><Link to="/Goote" spy={true} smooth={true}>서비스</Link></li>
              {/* <li><NavLink exact to="/">홈</NavLink></li>
              <li><NavLink exact to="/goote">서비스</NavLink></li>
              <li><NavLink exact to="/profile">회사 소개</NavLink></li>
              <li><NavLink exact to="/notice">공지사항</NavLink></li> */}
            </ul>
          </BrowserRouter>

          <input type="checkbox" id="menuicon"/>
          <label htmlFor="menuicon">
            <span></span>
            <span></span>
            <span></span>
          </label>
          <div className="sidebar">
            <BrowserRouter>
              <ul className="sidebar__menu">
                <li><Link to="/" spy={true} smooth={true}>홈</Link></li>
                <li><Link to="/Goote" spy={true} smooth={true}>서비스</Link></li>
                {/* <li><NavLink exact to="/">홈</NavLink></li>
                <li><NavLink exact to="/goote">서비스</NavLink></li>
                <li><NavLink exact to="/profile">회사 소개</NavLink></li>
                <li><NavLink exact to="/notice">공지사항</NavLink></li> */}
              </ul>
            </BrowserRouter>
          </div>
        </nav>
      </div>
    );
}

export default Navigation;