import { Component } from 'react';
import './App.css';
import Home from './pages/Home.jsx';
import Profile from './pages/Profile.jsx';
import Goote from './pages/Goote.jsx';
import Notice from './pages/Notice.jsx';
import Footer from './components/Footer.jsx';
import Navigation from './components/Navigation.jsx';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

class App extends Component {
  render(){
    return(
      <div className="App">
        <BrowserRouter>
          <Navigation></Navigation>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/goote" component={Goote} />
            <Route path="/profile" component={Profile} />
            <Route path="/notice" component={Notice} />
          </Switch>
          <Footer></Footer>
        </BrowserRouter>
      </div>
    );
  }

}

export default App;
