import { Component } from 'react';
import '../styles/css/Profile.css';
import '../styles/css/common.css';
import banner from '../assets/banner.png';


class Profile extends Component {
  render(){
    return(
      <div className="Profile">
        <div className="square-background">
          <img src={banner} className="banner" alt="banner" />
        </div>
      </div>
    );
  }
}

export default Profile;