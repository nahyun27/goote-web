import { Component } from 'react';
import '../styles/css/Goote.css';
import '../styles/css/common.css';
import mockup_home from '../assets/home_mockup.png';
import mockup_apply from '../assets/apply_mockup.png';
import mockup_map from '../assets/map_mockup.png';

import matching from '../assets/tennis.png';
import assignment from '../assets/assign.png';
import map from '../assets/map.png';




class Goote extends Component {
  render(){
    return(
      <div className="Goote">
        <div className="home__main">
        <div className="function">
        <p className="sub-heading">“에이~ 클릭 몇번으로 게임이 성사된다고?”</p>
          <div className="text-top2">
            굿테와 함께하는
            <br/>
            너~무 간단한 테니스 친구 찾기!
          </div> 
          <div className="items">
            <div className="item">
              <img src={matching} className="item-icon" alt="item-icon" />
              <div className="title">테니스 게임 매칭</div>
              <p>이보다 빠른 매칭은 없다!</p>
              <p>너무 간단한 게임 개설/신청</p>
              
            </div>
            <div className="item">
              <img src={assignment} className="item-icon" alt="item-icon" />
              <div className="title">테니스 코트 양도</div>
              <p>예약한 코트가 필요 없어졌다구요?</p>
              <p>굿테에서 쉽고 빠르게 양도해요~</p>
              
            </div>
            <div className="item">
              <img src={map} className="item-icon" alt="item-icon" />
              <div className="title">코트 찾기</div>
              <p>“우리집 주변에 개설된 게임 없나?”</p>
              <p>직관적인 위치기반 게임/양도 검색! </p>
            </div>
          </div>
        </div>

        <div className="page">
          <div className="heading">
            <p className="sub-heading">번거로운 모집/양도는 그만!</p>
            <p className="main-heading">어플 하나로</p>
            <p className="main-heading">빠르고 편하게 여가생활 즐기기!</p>
          </div>
          <div className="square">
            <span className="square"></span>
          </div>
          <div className="content">
            <img src={mockup_home} className="mockup" alt="mockup" />
            <div className="content-text">
              <div className="text-top">No.1 테니스 게임 매칭 플랫폼!</div>
              <div className="floor">
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">검증된 NTRP</b>
                  </div>
                  <div className="line">
                  게임 후 리뷰를 통한 NTRP 정확도 향상
                  </div>
                  <div className="line">
                  검증된 사람들끼리 테니스 함께 쳐요~
                  </div>
                </div>
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">No show 방지</b>
                  </div>
                  <div className="line">
                  수수료 패널티와 신고 정책,
                  </div>
                  <div className="line">
                  선결제 시스템으로 인한 no show율 감소!
                  </div>
                </div>
              </div>
              <div className="floor">
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">1초 개임 개설!</b>
                  </div>
                  <div className="line">
                  자주가는 테니스장, 자주 쓰는 옵션
                  </div>
                  <div className="line">
                  불러오기 기능을 통해 빠르게 개설!
                  </div>
                </div>
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">안전한 결제</b>
                  </div>
                  <div className="line">
                  게임 완료 후 호스트에게 지급!
                  </div>
                  <div className="line">
                  계좌 번호 유출 걱정 끝~ 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="page2">
          <div  className="heading2">
            <p className="sub-heading">게임 개설부터 모집까지!</p>
            <p className="main-heading">굿테가 다~ 관리해주니까!</p>
          </div>
          <div className="content">
            <img src={mockup_apply} className="mockup mockup-mobile" alt="mockup2" />
            <div className="content-text">
              <div className="text-top">내 마음 알아주는 똑똑한 플랫폼!</div>
              <div className="floor">
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">위치 기반 게임 검색</b>
                  </div>
                  <div className="line">
                  내 위치 기반의 가까운 게임 정보가 내손에!
                  </div>
                </div>
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">개인정보 보호</b>
                  </div>
                  <div className="line">
                  전화번호, 계좌번호 노출 걱정은 그만!
                  </div>
                  <div className="line">
                  굿테를 통해 모든 개인정보를 보호하세요!
                  </div>
                </div>
              </div>
              <div className="floor">
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">게스트 자동 모집</b>
                  </div>
                  <div className="line">
                  "게스트가 갑자기 게임을 취소하면 어쩌지?"
                  </div>
                  <div className="line">
                  굿테에서 게스트를 자동 모집해줍니다!
                  </div>
                </div>
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">AI 매칭 기능</b>
                  </div>
                  <div className="line">
                  비슷한 NTRP나 리뷰를 통한 선호도 분석을 통한
                  </div>
                  <div className="line">
                  테니스 파트너 매칭 기능
                  </div>
                </div>
              </div>
            </div>
            <img src={mockup_apply} className="mockup mockup-web" alt="mockup2" />
          </div>
        </div>

        <div className="page">
          <div className="heading">
            <p className="sub-heading">이렇게 편한 게임 신청!</p>
            <p className="main-heading">입맛에 맞는 게임 빠르게 찾자!</p>
          </div>
          <div className="content">
            <img src={mockup_map} className="mockup" alt="mockup3" />
            <div className="content-text">
            <div className="text-top">No.1 테니스 게임 매칭 플랫폼!</div>
              <div className="floor">
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">직관적인 위치기반 게임 조회</b>
                  </div>
                  <div className="line">
                  언제 어디서든, 내 주변에 개설된
                  </div>
                  <div className="line">
                  게임/양도 현황을 한눈에 파악하자!
                  </div>
                </div>
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">지도 위 게임 상세정보</b>
                  </div>
                  <div className="line">
                  지도 상 아이콘을 클릭하면
                  </div>
                  <div className="line">
                  게임/양도 정보 바로 조회 가능!
                  </div>
                </div>
              </div>
              <div className="floor">
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">개인정보 보호</b>
                  </div>
                  <div className="line">
                  전화번호, 계좌번호 노출 걱정은 그만!
                  </div>
                  <div className="line">
                  굿테를 통해 모든 개인정보를 보호하세요!
                  </div>
                </div>
                <div className="texts">
                  <div className="line-top">
                    <b className="bold">숨어있는 코트</b>
                  </div>
                  <div className="line">
                  내가 모르는 우리동네 숨어있는 코트
                  </div>
                  <div className="line">
                  굿테에서 찾아드립니다.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        
      </div>
    );
  }
}

export default Goote;