import React from "react"
import Home from './Home.jsx';
import Goote from './Goote.jsx';
import Profile from './Profile.jsx';

const Middle = () => {
  return (
    <div className="Middle">
      <div id="/">
        <Home></Home>
      </div>
      
      <div id="/Goote">
        <Goote></Goote>
      </div>

      <div id="/Profile">
        <Profile></Profile>
      </div>
    </div>
  )
}

export default Middle