import '../styles/css/Home.css';
import '../styles/css/common.css';
import background from '../assets/background.png';

function Home (){
  return(
    <div className="Home">
      <div className="top__background">
        <img src={background} className="background__img" alt="background" />
      </div>
      <div className="top__contents">
        <p className="sub-text">전국 테니스인들을 하나로 모으다!</p>
        <p className="main-text">쉽고 편한 <b className="emphasize">테니스 친구</b> 찾기</p>
        <p className="main-text-goote">굿테</p>
      </div>
    </div>
  );
}

export default Home;